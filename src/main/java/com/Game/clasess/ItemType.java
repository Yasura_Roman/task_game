package com.Game.clasess;

public enum ItemType {
    PECTORAL_ARMOR,
    HELMET,
    RING,
    WEAPON,
    SHIELD;
}
